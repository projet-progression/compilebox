#!/bin/bash
&>/tmp/startup.log python3 /usr/src/app/api.py&

for i in $(seq 0 30);
do
	if bash -c "/usr/bin/curl localhost:80"
	then
		$*
		exit 0
	else
		sleep 1
	fi
done

>&2 echo Erreur. Flask n'a pas démarré après 30s
>&2 cat /tmp/startup.log
exit 1
