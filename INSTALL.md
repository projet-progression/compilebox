# Compilebox

## 1. Dépendances obligatoires

- [git](https://git-scm.com/downloads)
- [docker](https://www.docker.com/)

## 2. Installation & Configuration

### Obtenir le code source

Cloner le projet **compilebox**

```
git clone https://git.dti.crosemont.quebec/progression/compilebox.git
cd compilebox
```

### Créer et adapter le fichier de configuration

Créer le fichier .env ou copier le ficher d'exemple `env.exemple`

```
cp env.exemple .env
```

Modifier les options de configuration minimales :

#### INSTANCE_PORT

Numéro de port de l'hôte sur lequel le service écoute.


### Démarrer l'application

Démarrage du conteneur `compilebox`

```
docker-compose up -d compilebox
```

L'application est accessible via localhost

```
$ source .env
$ curl \
   -H "Content-Type: application/json" \
   --data '
   {
     "vm_name":"$CI_REGISTRY/$CI_PROJECT_PATH/compilebox/remotecompiler:latest",
	 "language": 1,
	 "code":"print(\"Hello world\")",
	 "tests":[ { "sortie_attendue": "Hello world" } ]
    }
   ' \
   http://localhost:$INSTANCE_PORT/compile
```
