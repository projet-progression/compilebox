#!/bin/bash

contid=$1

user="$2"

cmd="$3"

port=$(comm -23 <(seq 32768 35768 | sort) <(ss -Htan | awk '{print $4}' | cut -d':' -f2 | sort -u) | shuf | head -n 1)

2>&1 tty-share \
	--listen "localhost:$port" \
	--public \
	--tty-proxy ${TTYSHARE_PROXY_URL:-ttyshare-proxy:3456} \
	--headless \
	--headless-cols 120\
	--headless-rows 25\
	--no-wait \
	--no-tls \
	--subdir "$TTYSHARE_SUBDIR" \
	--command docker \
	--args "exec -it -u $user $contid $cmd" | tee /tmp/tty_share_$contid.log|grep --line-buffered -oP '(?<=public session: )http.*$' > /tmp/tty_share_$contid.url

rm /tmp/tty_share_$contid* /tmp/init_$contid
