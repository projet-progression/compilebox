&>/tmp/startup.log /usr/local/bin/docker-entrypoint.sh mariadbd&

for i in $(seq 0 30);
do
	if bash -c "echo exit|mariadb -uroot -ppassword&>/tmp/startup.log"
	then
		exit 0
	fi
	sleep 1
done

echo Erreur. Mariadb n\'a pas démarré après 30s
cat /tmp/startup.log
exit 1
