#!/bin/bash

export PYTHONIOENCODING=utf-8

########################################################################
#	- This is the main script that is used to compile/interpret the source code
#	- The script takes 3 arguments
#		1. The compiler that is to compile the source file.
#		2. The source file that is to be compiled/interpreted
#		3. Additional argument only needed for compilers, to execute the object code
#	
#	- Sample execution command:   $: ./script.sh g++ file.cpp ./a.out
#	
########################################################################

compiler=$1
file=$2
output=$3
addtionalArg=$4
if [ -z $CB_TIMEOUT_PS ]
then
    temps_max=5
else
    temps_max=$CB_TIMEOUT_PS
fi

########################################################################
#	- The script works as follows
#	- It first stores the stdout and std err to another stream
#	- The output of the stream is then sent to respective files
#	
#	
#	- if third arguemtn is empty Branch 1 is followed. An interpretor was called
#	- else Branch2 is followed, a compiler was invoked
#	- In Branch2. We first check if the compile operation was a success (code returned 0)
#	
#	- If the return code from compile is 0 follow Branch2a and call the output command
#	- Else follow Branch2b and output error Message
#	
#	- Stderr and Stdout are restored
#	- Once the logfile is completely written, it is renamed to "completed"
#	- The purpose of creating the "completed" file is because NodeJs searches for this file 
#	- Upon finding this file, the NodeJS Api returns its content to the browser and deletes the folder
#
#	
########################################################################

#3>&1 4>&2 >

#Branch 1
START_TOTAL=$(date +%s.%3N)
if [ "$output" = "nul" ]; then
	for filename in $(ls -v /usercode/inputFile.d)
	do
		touch $"/usercode/outputFile.d/$filename"
		exec  1> $"/usercode/outputFile.d/$filename"
		exec  2> $"/usercode/outputFile.d/$filename.errors"

		START=$(date +%s.%3N)
		timeout $temps_max $compiler /usercode/$file $(cat "/usercode/paramsFile.d/$filename") < "/usercode/inputFile.d/$filename"

		END=$(date +%s.%3N)
		runtime=$(echo "$END - $START" | bc)
		if (( $( echo "$runtime >= $temps_max" | bc -l ) ))
		then
			>&2 echo "TIMEOUT ( > ${temps_max}s)"
		fi
		echo "*-COMPILEBOX::ENDOFOUTPUT-*" $runtime 

	done
#Branch 2
else
	#In case of compile errors, redirect them to a file
	START=$(date +%s.%3N)
	$compiler /usercode/$file $addtionalArg &> /usercode/errors.txt
	
	#Branch 2a
	if [ $? -eq 0 ];	then
		for filename in $(ls -v /usercode/inputFile.d)
		do
			touch $"/usercode/outputFile.d/$filename"
			exec  1> $"/usercode/outputFile.d/$filename"
			cp /usercode/errors.txt "/usercode/outputFile.d/$filename.errors"
			exec  2>> $"/usercode/outputFile.d/$filename.errors"

			START=$(date +%s.%3N)
			timeout $temps_max $output $(cat "/usercode/paramsFile.d/$filename") < "/usercode/inputFile.d/$filename"

			END=$(date +%s.%3N)
			runtime=$(echo "$END - $START" | bc)
			if (( $( echo "$runtime >= $temps_max" | bc -l ) ))
			then
				>&2 echo "TIMEOUT ( > ${temps_max}s)"
			fi
			echo "*-COMPILEBOX::ENDOFOUTPUT-*" $runtime 

			done
#Branch 2b
	else
		for filename in $(ls -v /usercode/inputFile.d)
		do
			exec  1> $"/usercode/outputFile.d/$filename"
			echo "Compilation Failed"

			END=$(date +%s.%3N)
			runtime=$(echo "$END - $START" | bc)
			echo "*-COMPILEBOX::ENDOFOUTPUT-*" $runtime 
			#if compilation fails, display the output file	
			cat /usercode/errors.txt > "/usercode/outputFile.d/$filename.errors"
		done
	fi
fi

END_TOTAL=$(date +%s.%3N)
runtime_total=$(echo "$END_TOTAL - $START_TOTAL" | bc)
echo $runtime_total > /usercode/completed

