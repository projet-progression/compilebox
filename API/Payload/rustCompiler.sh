#!/usr/bin/env sh

export RUSTUP_HOME=/opt/rust
export CARGO_HOME=/opt/rust
export PATH=$PATH:$CARGO_HOME/bin

cd /usercode/
rustc -o main $1
