#!/bin/bash
source /home/user/.sdkman/bin/sdkman-init.sh
cd /usercode

JUNIT=/usr/share/java/junit4.jar
export CLASSPATH=$CLASSPATH:$JUNIT:.

kotlinc -cp $CLASSPATH $1
