const readfile = require("fs").readFileSync;
const input = readfile(0).toString().split(/[\n\r]/g);
const len = input.length - (input.at(-1)=='' ? 1 : 0);
var it = 0;
global.input = () => {
	if(it>=len) throw new Error("Lecture au-delà de la fin du fichier.");
	return input[it++];
};
