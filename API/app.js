#!/usr/bin/node
/*
 *File: app.js
 *Author: Asad Memon / Osman Ali Mian
 *Last Modified: 5th June 2014
 *Revised on: 30th June 2014 (Introduced Express-Brute for Bruteforce pnotection)
 */


var express = require("express");
var arr = require("./compilers");
var sandBox = require("./DockerSandbox");
var app = express();
const port=process.env.CB_PORT ?? 12380;
const timeout_comp = process.env.CB_TIMEOUT_COMP ?? 30;
const timeout_cont = process.env.CB_TIMEOUT_CONT ?? 600;

//var ExpressBrute = require('express-brute');
//var store = new ExpressBrute.MemoryStore(); // stores state locally, don't use this in production
//var bruteforce = new ExpressBrute(store,{
//	freeRetries: 2,
//	minWait: 500
//});
//
app.use(express.static(__dirname));
app.use(express.urlencoded( { limit: '1mb', extended: true, parameterLimit: 1000 }));
app.use(express.json({limit: '1mb'}));

app.all("*", function(req, res, next)
{
	res.header("Access-Control-Allow-Origin", "*");
	res.header("Access-Control-Allow-Methods", "PUT, GET, POST, DELETE, OPTIONS");
	res.header("Access-Control-Allow-Headers", "Content-Type");

	next();
});

function random(size) {
	//returns a crypto-safe random
	return require("crypto").randomBytes(size).toString("hex");
}


app.post("/compile",function(req, res) {
	var language = parseInt(req.body.language);
	if(isNaN(language)) {
		for( var i=0; i<arr.compilerArray.length; i++ ){
			if(arr.compilerArray[i][0] === req.body.language)
				language = i;
			break;
		}
	}
	var code = req.body.code ?? "";

	var tests=[];
	var parameters=req.body.parameters;

	if ("tests" in req.body){
		tests = req.body.tests;
	}

	var params_conteneur = req.body.params_conteneur;

	var folder= "temp/" + random(10); //folder in which the temporary folder will be saved
	var path=__dirname+"/"; //current working path

	var vm_name=req.body.vm_name; //name of virtual machine that we want to execute

	var timeout_value;

	if ( !vm_name && code != "reset" ){
		res.status(400).send("VM invalide");
		return;
	}
	if ( isNaN(language) || language < 0 || language > arr.compilerArray.length - 1 ){
		res.status(400).send("Langage invalide");
		return;
	}
	if ( arr.compilerArray[language][0] != "sshd" ){
		if ( !tests || tests.length < 1 ){
			res.status(400).send("Tests invalide");
			return;
		}

		timeout_value=timeout_comp;//Timeout Value, In Seconds
	}
	else {
		timeout_value=timeout_cont;//Timeout Value, In Seconds
	}

	var user = req.body.user;
	var user_cmd = req.body.user_cmd;

	//details of this are present in DockerSandbox.js
	var sandboxType = new sandBox(timeout_value,path,folder,parameters,vm_name,params_conteneur, arr.compilerArray[language][0],arr.compilerArray[language][1],code,arr.compilerArray[language][2],arr.compilerArray[language][3],arr.compilerArray[language][4],tests,user,user_cmd);

	//data will contain the output of the compiled/interpreted code
	//the result maybe normal program output, list of error messages or a Timeout error
	if(arr.compilerArray[language][0]=="sshd"){
		sandboxType.run(function(contid, path, résultats)
		{
			res.send({résultats: résultats, conteneur: {id: contid, path: path}});
		},
		function(code, message){
			res.status(code).send({message: message});
		});
	}
	else{
		sandboxType.run(function(résultats, exec_time)
		{
			//Terrible hack pour empêcher le crash. À RÉGLER!!!
			try{
				//console.log("Data: received: "+ data)
				res.send({résultats: résultats,temps_exec: exec_time});
			}
			catch(err){
				console.log(err);
			}
		},
		function(code, message){
			res.status(code).send({message: message});
		});
	}

});

console.log("Listening at "+port);

//N'accepte que les connexions locales
app.listen(port, "0.0.0.0");
