/*
 *File: DockerSandbox.js
 *Author: Osman Ali Mian/Asad Memon
 *Created: 3rd June 2014
 *Revised on: 25th June 2014 (Added folder mount permission and changed executing user to nobody using -u argument)
 *Revised on: 30th June 2014 (Changed the way errors are logged on console, added language name into error messages)
 */


/**
 * @Constructor
 * @variable DockerSandbox
 * @description This constructor stores all the arguments needed to prepare and execute a Docker Sandbox
 * @param {Number} timeout_value: The Time_out limit for code execution in Docker
 * @param {String} path: The current working directory where the current API folder is kept
 * @param {String} folder: The name of the folder that would be mounted/shared with Docker container, this will be concatenated with path
 * @param {String} vm_name: The TAG of the Docker VM that we wish to execute
 * @param {String} compiler_name: The compiler/interpretor to use for carrying out the translation
 * @param {String} file_name: The file_name to which source code will be written
 * @param {String} code: The actual code
 * @param {String} output_command: Used in case of compilers only, to execute the object code, send " " in case of interpretors
 */
var DockerSandbox = function(timeout_value,path,folder,parameters,vm_name,params_conteneur,compiler_name,file_name,code,output_command,languageName,e_arguments,tests,user, user_cmd)
{

	this.timeout_value=timeout_value;
	this.path=path;
	this.folder=folder;
	this.parameters=parameters;
	this.params_conteneur = params_conteneur ?? "";
	this.vm_name = vm_name;
	this.compiler_name=compiler_name;
	this.file_name=file_name;
	this.code = code;
	this.output_command=output_command;
	this.langName=languageName;
	if(output_command=="nul"){
		this.extra_arguments=parameters;
	}
	else{
		this.extra_arguments=e_arguments;
	}
	this.tests=tests;
	this.user=user ? user : "root";
	this.user_cmd=user_cmd ? user_cmd : "sh";
};


/**
 * @function
 * @name DockerSandbox.run
 * @description Function that first prepares the Docker environment and then executes the Docker sandbox 
 * @param {Function pointer} success ?????
 */
DockerSandbox.prototype.run = function(success, err)
{
	var sandbox = this;

	if(this.compiler_name=="sshd" && get_conteneur_from_id( this.parameters ) ){
		//Exécution d'un conteneur existant
		sandbox.execute(success, err);
	}
	else{
		this.prepare( function(){
			sandbox.execute(success, err);
		});
	}
};


/*
 * @function
 * @name DockerSandbox.prepare
 * @description Function that creates a directory with the folder name already provided through constructor
 * and then copies contents of folder named Payload to the created folder, this newly created folder will be mounted
 * on the Docker Container. A file with the name specified in file_name variable of this class is created and all the
 * code written in 'code' variable of this class is copied into this file.
 * Summary: This function produces a folder that contains the source file and 2 scripts, this folder is mounted to our
 * Docker container when we run it.
 * @param {Function pointer} success ?????
 */
DockerSandbox.prototype.prepare = function(success)
{
	var execSync = require("child_process").execSync;
	var fs = require("fs");
	var sandbox = this;

	var cmd = "mkdir -p $USERCODE/inputFile.d $USERCODE/paramsFile.d $USERCODE/outputFile.d && cp $CB_PATH/Payload/* $USERCODE && chmod 777 -R $USERCODE";
	execSync(cmd, {env: {...process.env, CB_PATH: sandbox.path, USERCODE: sandbox.path + sandbox.folder }}).toString();

	fs.writeFileSync(sandbox.path + sandbox.folder+"/" + sandbox.file_name, sandbox.code);

	console.log(sandbox.langName+" file was saved!");
	execSync("chmod 777 \"$FILENAME\"", {env: {...process.env, FILENAME: sandbox.path+sandbox.folder+"/"+this.file_name}});

	var i=0;
	for(var test in sandbox.tests){
		fs.writeFileSync(sandbox.path + sandbox.folder+"/inputFile.d/"+i, "stdin" in sandbox.tests[test] ? sandbox.tests[test].stdin : "");
		console.log("Input file was saved!");
		fs.writeFileSync(sandbox.path + sandbox.folder+"/paramsFile.d/"+i, "params" in sandbox.tests[test] ? sandbox.tests[test].params : "");
		console.log("Params file was saved!");

		i+=1;
	}

	success();

};

function get_conteneur_from_id( id ){
	var execSync = require("child_process").execSync;
	try{
		return execSync("docker inspect $ID", {env: {...process.env, ID: id}});
	}
	catch(err){
		return false;
	}
}

/*
 * @function
 * @name DockerSandbox.execute
 * @precondition: DockerSandbox.prepare() has successfully completed
 * @description: This function takes the newly created folder prepared by DockerSandbox.prepare() and spawns a Docker container
 * with the folder mounted inside the container with the name '/usercode/' and calls the script.sh file present in that folder
 * to carry out the compilation. The Sandbox is spawned ASYNCHRONOUSLY and is supervised for a timeout limit specified in timeout_limit
 * variable in this class. This function keeps checking for the file "Completed" until the file is created by script.sh or the timeout occurs
 * In case of timeout an error message is returned back, otherwise the contents of the file (which could be the program output or log of 
 * compilation error) is returned. In the end the function deletes the temporary folder and exits
 * 
 * Summary: Run the Docker container and execute script.sh inside it. Return the output generated and delete the mounted folder
 *
 * @param {Function pointer} success ?????
 */

DockerSandbox.prototype.executer_conteneur=async function(success, err){
	var exec = require("child_process").exec;
	var execSync = require("child_process").execSync;
	var spawn = require("child_process").spawn;
	var fs = require("fs");
	var writeFile = require("fs/promises").writeFile;
	
	var parameters = this.parameters;
	var ttyshare_path = "";
	
	//Cherche le conteneur auquel on veut se connecter
	if(get_conteneur_from_id(parameters)){
		console.log("Resume : " + parameters);
		contid = parameters;
	}
	else{
		console.log("Démarrage d'un conteneur " + this.vm_name );
		var st="";
		if(this.vm_name=="redirect"){
			st = "bash -c \"docker run --rm --cap-add=NET_ADMIN -h $VM_NAME -d $VM_NAME \"";
		}
		else{
			st = "bash -c \"docker run --rm --memory=512m --memory-swap=512m --cpus=2 $PARAMS_CONTENEUR -v  $CB_PATH/$CB_FOLDER:/usercode -d $VM_NAME\"";
		}


		try{
		//execute the Docker
			console.log(`Pulling ${this.vm_name}`);
			execSync("docker pull $VM_NAME", {env: {...process.env, VM_NAME: this.vm_name}});

			//log the statement in console
			console.log("Executing container");
			var contid=execSync(st, {env: {...process.env, VM_NAME: this.vm_name, PARAMS_CONTENEUR: this.params_conteneur, CB_PATH: this.path, CB_FOLDER: this.folder }}).toString().trim();
			console.log("contid: " + contid);
		}
		catch(e){
			console.log("Erreur : " + e);
			err(500, e.toString());
			return 0;
		}

		// on attend 30s que le conteneur soit viable
		var i=0;
		for(i=0;i<30;i++){
			try{
				const status = execSync("docker inspect --format='{{json .State.Health.Status}}' " + contid);
				if(status == "\"healthy\"\n") break;
			}
			catch{
				//Continuons d'attendre
			}
			await new Promise(r => setTimeout(r, 1000));
		}

		// Copie le script d'initialisation
		await writeFile("/tmp/init_" + contid, this.code);
		try {
		//Exécute le script d'initialisation
			execSync("docker cp /tmp/init_" + contid + " " + contid + ":/tmp/init.sh");
			execSync("docker exec " + contid + " sh /tmp/init.sh");
			console.log("------------------------------");
			console.log("Created : "+contid);
		}
		catch(e){
			console.log("Erreur : " + e);
			err(400, "Erreur d'initialisation" + e.toString());
			return 0;
		}

		//Prévoit la suppression des fichiers et l'arrêt du conteneur après le timeout
		setTimeout( () => {
			console.log("ATTEMPTING TO REMOVE: " + this.folder);
			console.log("------------------------------");
			exec("rm -r " + this.folder);
			exec("docker stop " + contid);
		}, this.timeout_value * 1000);
	}

	// Démarre ou redémarre TTY-Share s'il s'est arrêté
	if(!fs.existsSync("/tmp/tty_share_" + contid + ".url" )){
		console.log("Démarrage de TTYShare");
		spawn("bash", ["/exec_ttyshare.sh", contid, this.user, this.user_cmd]);
	}

	//On attend que l'url du terminal soit disponible, max 5s
	for(i = 0;i<5;i++){
		if(fs.existsSync("/tmp/tty_share_" + contid + ".url" )){
			ttyshare_path = fs.readFileSync("/tmp/tty_share_" + contid + ".url").toString().trim();
			if(ttyshare_path!="") break;
		}
		await new Promise(r => setTimeout(r, 1000));
	}
	if(i==50){
		err(500, "TTY-share n'a pas démarré");
		return 0;
	}
	
	// Exécution des tests
	try{
		var résultats=[];

		for( var no_test=0; no_test < this.tests.length; no_test++) {
		//Exécute les tests
			résultats.push( exécuter_test(no_test) );
		}

	}
	catch(e){
		console.log("Erreur : " + e);
		err(500, e.toString());
		return 0;
	}

	success(contid, ttyshare_path, résultats);
	return contid;

	function exécuter_test( no_test ){
		const code_exec="docker exec " + contid + " bash /usercode/inputFile.d/" + no_test;

		var code_res = 0;
		var stdout;
		var stderr;
		try {
			stdout = execSync(code_exec, {shell: "/bin/bash"}).toString();
			code_res = 0;
		}
		catch(e) {
			code_res = e.status;
			stderr = e.stderr.toString();
			stdout = e.stdout.toString();
		}

		return { output: stdout, errors: stderr, code: code_res, time: 0 };
	}
};

DockerSandbox.prototype.executer_compilation=function(success, err){
	var exec = require("child_process").exec;
	var execSync = require("child_process").execSync;
	var fs = require("fs");

	var sandbox=this;

	var myC = 0; //variable to enforce the timeout_value
	//this statement is what is executed
	var user = this.langName == "SQL" ? "mysql" : "user";

	var st = this.path+"DockerTimeout.sh " + this.timeout_value + "s --rm -u " + user + " -e 'NODE_PATH=/usr/local/lib/node_modules' -e LANG=fr_CA.utf8 -i -t  --net=none --memory=512m --memory-swap=512m --cpus=2 -v  $CB_PATH/$CB_FOLDER:/usercode $VM_NAME /usercode/script.sh \"$CMP_NAME\" \"$FILENAME\" \"$CMD\" \"$ARGS\"";

	try{
		console.log(`Pulling ${this.vm_name}`);
		execSync("docker pull $VM_NAME", {env: {...process.env, VM_NAME: this.vm_name}});

		//log the statement in console
		console.log(`Executing ${st}`);
		exec(st, {env: {...process.env,
		                VM_NAME: this.vm_name,
		                CB_PATH: this.path,
		                CB_FOLDER: this.folder,
		                ARGS: this.extra_arguments,
		                CMP_NAME: this.compiler_name,
		                FILENAME: this.file_name,
		                CMD: this.output_command }});
	}
	catch(e){
		console.log("Erreur : " + e);
		err(500, e.toString());
		return 0;
	}
	
	console.log("------------------------------");
	//Check For File named "completed" after every 1 second

	const intid = setInterval( () =>
	{
		myC = myC + 1;

		if (myC < sandbox.timeout_value*10 && ! fs.existsSync (sandbox.path + sandbox.folder + "/completed")) {
			return;
		}

		if (myC >= sandbox.timeout_value*10 ){
			console.log("TIMEOUT");
		}

		clearInterval(intid);

		var résultats=[];

		for(var test in sandbox.tests){
		//Stdout
			try{
				var data = fs.readFileSync(sandbox.path + sandbox.folder + "/outputFile.d/" + test, "utf8");
				var lines = data.toString().split("*-COMPILEBOX::ENDOFOUTPUT-*");
				data=lines[0];
				var time=parseFloat(lines[1]);
				résultats[test] = {
					output: data,
					errors: "",
					time: time
				};
			}
			catch(err){
				résultats[test] = {
					output: "",
					errors: "TIMEOUT",
					time: 0
				};
			}

			// Stderr
			try{
				var data_err = fs.readFileSync(sandbox.path + sandbox.folder + "/outputFile.d/" + test + ".errors", "utf8");
				résultats[test].errors = data_err;
			}
			catch(err){
				//Pas de fichier d'erreur
			}
		}

		var temps_exec = sandbox.timeout_value;
		if(fs.existsSync( sandbox.path + sandbox.folder + "/completed")){
			temps_exec = parseFloat(fs.readFileSync( sandbox.path + sandbox.folder + "/completed", "utf8"));
		}

		success(résultats, temps_exec);

		//now remove the temporary directory
		console.log("ATTEMPTING TO REMOVE: " + sandbox.folder);
		console.log("------------------------------");
		exec("rm -r $DIR", {env: {DIR: sandbox.folder}});

	}, 100);
};


DockerSandbox.prototype.execute = function(success, err)
{
	var execSync = require("child_process").execSync;

	console.log( new Date() );
	try{
		if(this.compiler_name=="sshd"){
			const id = this.parameters;
			if(this.code=="reset") {
				console.log("RESET " + id);
				if(get_conteneur_from_id(id)){
					try{
						console.log(`On arrête le conteneur ${id}`);
						execSync(`docker stop ${id}`);
					}
					catch(err){
						console.log(`Conteneur ${id} n'existe pas`);
					}
				}
				success();
			}
			else{
				this.executer_conteneur(success, err);
			}
		}
		//Sinon, une compilation ordinaire
		else{
			this.executer_compilation(success, err);
		}
	}catch(err){
		console.log(err);
	}
};

module.exports = DockerSandbox;
